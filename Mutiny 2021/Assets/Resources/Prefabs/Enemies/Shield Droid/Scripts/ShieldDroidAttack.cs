using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldDroidAttack : EnemyAttack
{
    [SerializeField] private GameObject shield;
    [SerializeField] private GameObject attackPoint;
    [SerializeField] private float timeOpenShield;
    [SerializeField] private float inTimeOpenShield;
    [SerializeField] private Vector2 attackZone;

    public override int SetIndexAttack()
    {
        if(_attackIndex == 0)
        {
            _attackIndex = 1;
            OpenShield();
            return 0;
        }
        else if(_attackIndex == 1)
        {
            return 1;
        }
        return 0;
    }

    private void OpenShield()
    {
        anim.SetBool("isOpenShield", true);
        inTimeOpenShield = timeOpenShield;
        shield.SetActive(true);
    }

    private void CloseShield()
    {
        _waitTimeBreakAfterAttack = _timeBreakAfterAttack;
        anim.SetBool("isAttack", false);
        anim.SetBool("isOpenShield", false);
        shield.SetActive(false);
    }

    public override void Attack(int _attackIndex)
    {
        Collider2D[] colliderCount = Physics2D.OverlapBoxAll(attackPoint.transform.position, attackZone, 0f, enemyLayers);
        for (int i = 0; i < colliderCount.Length; i++)
        {
            if (!collidersDamaged.Contains(colliderCount[i]) && colliderCount[i].TryGetComponent(out BaseStats _theBS) && !_theBS.died)
            {
                if (InflictDamage(colliderCount[i])) collidersDamaged.Add(colliderCount[i]);
            }
        }
    }

    protected override void DoneAttackAnim()
    {
        _attackIndex = 0;
        base.DoneAttackAnim();
    }

    protected override void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(attackPoint.transform.position, attackZone);
        base.OnDrawGizmosSelected();
    }
}
