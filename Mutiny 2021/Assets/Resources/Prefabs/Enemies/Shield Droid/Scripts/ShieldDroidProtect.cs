using UnityEngine;

public class ShieldDroidProtect : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Victim _vic))
        {
            _vic.isInvicible = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Victim _vic))
        {
            _vic.isInvicible = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Victim _vic))
        {
            _vic.isInvicible = false;
        }
    }
}
