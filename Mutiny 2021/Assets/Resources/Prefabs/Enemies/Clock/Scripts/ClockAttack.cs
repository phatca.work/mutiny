using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockAttack : EnemyAttack
{
    private Rigidbody2D theRB;
    private EnemyMovement theEM;

    protected override void Start()
    {
        base.Start();
        theRB = GetComponent<Rigidbody2D>();
        theEM = GetComponent<EnemyMovement>();
    }

    public override int SetIndexAttack()
    {
        return 0;
    }

    protected override void Update()
    {
        base.Update();
        Attack();
    }

    private void Attack()
    {
        if (anim.GetBool("inAttack"))
        {
            if (Physics2D.OverlapCircle(theEM.facePoint.position, .2f, theEM.whichIsTouched))
            {
                DoneAttackAnim();
            }
            theRB.velocity = new Vector2(transform.rotation.y == 0 ? theBS.moveSpeed.GetValue() * 1.5f : -theBS.moveSpeed.GetValue() * 1.5f, 0f);
        }
    }

    public override bool CheckAttack()
    {
        if (RestAfterAttack()) return true;
        if (anim.GetBool("isAttack")) return true;
        Collider2D enemy = Physics2D.OverlapBox(arrAttackPoint[_attackIndex].position, areaAttack, 0f, enemyLayers);
        if (enemy == null || enemy.GetComponent<BaseStats>().died)
        {
            return false;
        }
        StartAnimAttack(enemy.transform);
        return true;
    }

    protected override void DoneAttackAnim()
    {
        base.DoneAttackAnim();
        anim.SetBool("inAttack", false);
    }

    private void ClearColl()
    {
        collidersDamaged.Clear();
    }

    protected void StartAnimAttack(Transform target)
    {
        base.StartAnimAttack();
        transform.rotation = Quaternion.Euler(0, transform.position.x < target.position.x ? 0 : -180, 0);
    }

    private void SetAnimInAttack()
    {
        anim.SetBool("inAttack", true);
    }
}
