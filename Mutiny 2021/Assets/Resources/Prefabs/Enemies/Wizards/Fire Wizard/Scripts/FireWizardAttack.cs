using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWizardAttack : EnemyAttack
{
    [SerializeField] private GameObject GOFireBall;
    [SerializeField] private Transform[] positionSpawnSkill;
    [SerializeField] private List<FireBallOfFireWizard> arrFireBalls = new();
    private int countSpawnSkill;

    public override int SetIndexAttack()
    {
        return 0;
    }

    public override bool CheckAttack()
    {
        if (RestAfterAttack()) return true;
        Collider2D enemy = Physics2D.OverlapBox(arrAttackPoint[_attackIndex].position, areaAttack, 0f, enemyLayers);
        if (enemy == null || enemy.GetComponent<BaseStats>().currentHP.GetValue() <= 0) return false;
        CreateSkill(enemy);
        _waitTimeBreakAfterAttack = _timeBreakAfterAttack;
        return true;
    }

    private void CreateSkill(Collider2D enemy)
    {
        if(countSpawnSkill < 3)
        {
            GameObject fireBall = Instantiate(GOFireBall, positionSpawnSkill[countSpawnSkill].position, Quaternion.identity);
            fireBall.TryGetComponent(out FireBallOfFireWizard item);
            item.Setup(theBS.attackDamage.GetValue(), this);
            arrFireBalls.Add(item);
            countSpawnSkill++;
        }
        else
        {
            foreach (var item in arrFireBalls)
            {
                if (!item.isReady) return;
            }
            foreach (var item in arrFireBalls)
            {
                item.StartMove(enemy.transform);
            }
            countSpawnSkill = 0;
            arrFireBalls.Clear();
        }
    }

    public void DestroySkill()
    {
        foreach (var item in arrFireBalls)
        {
            item.Destroy();
        }
    }
}
