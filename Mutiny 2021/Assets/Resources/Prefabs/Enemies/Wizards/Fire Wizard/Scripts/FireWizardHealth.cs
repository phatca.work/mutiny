using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWizardHealth : EnemyHealth
{
    private FireWizardAttack theFWA;

    protected override void Start()
    {
        base.Start();
        theFWA = GetComponent<FireWizardAttack>();
    }

    protected override void Die()
    {
        theFWA.DestroySkill();
        base.Die();
    }
}
