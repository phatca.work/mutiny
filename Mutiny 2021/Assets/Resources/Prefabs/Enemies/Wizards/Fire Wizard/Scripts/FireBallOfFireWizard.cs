using UnityEngine;

public class FireBallOfFireWizard : Skill
{
    private Animator anim;
    public bool isReady;

    protected override void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
        theColl.enabled = false;
    }

    public void StartMove(Transform target)
    {
        theColl.enabled = true;
        Vector3 _direction = target.position - transform.position;
        theRB.velocity = new Vector2(_direction.x, _direction.y).normalized * speed;
    }

    private void CheckBallReady()
    {
        isReady = true;
    }

    public override void Destroy()
    {
        theRB.velocity = Vector2.zero;
        theColl.enabled = false;
        anim.SetTrigger("touch");
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Ground")))
        {
            Destroy();
        }
    }
}
