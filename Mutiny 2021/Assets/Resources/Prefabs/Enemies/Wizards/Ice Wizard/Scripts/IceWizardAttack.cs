using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceWizardAttack : EnemyAttack
{
    [SerializeField] private GameObject GOIce;
    private int countSpawnSkill;

    public override int SetIndexAttack()
    {
        return 0;
    }

    public override bool CheckAttack()
    {
        if (RestAfterAttack()) return true;
        Collider2D enemy = Physics2D.OverlapBox(arrAttackPoint[_attackIndex].position, areaAttack, 0f, enemyLayers);
        if (enemy == null || enemy.GetComponent<BaseStats>().currentHP.GetValue() <= 0) return false;
        _waitTimeBreakAfterAttack = _timeBreakAfterAttack;
        CreateSkill(enemy);
        return true;
    }

    private void CreateSkill(Collider2D enemy)
    {
        GameObject ice = Instantiate(GOIce, new Vector2(enemy.transform.position.x, enemy.transform.position.y + 7.5f), Quaternion.identity);
        
        ice.transform.eulerAngles = new Vector3(0, 0, -90);
        if (countSpawnSkill == 3)
        {
            ice.GetComponent<IceIceWizard>().Setup(theBS.attackDamage.GetValue(), this, true);
            ice.transform.localScale = new Vector2(3, 3);
            countSpawnSkill = 0;
        }
        else
        {
            ice.GetComponent<IceIceWizard>().Setup(theBS.attackDamage.GetValue(), this, false);
            countSpawnSkill++;
        }
    }
}
