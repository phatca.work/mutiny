using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceIceWizard : Skill
{
    private Animator anim;
    private bool isCritical;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
        theColl.enabled = false;
    }

    // Update is called once per frame

    private void Move()
    {
        theColl.enabled = true;
        theRB.velocity = new Vector2(0, -speed);
    }

    public void Setup(int _damage, Attacker _originOfDamage, bool _isCritical)
    {
        damage = _damage;
        originOfDamage = _originOfDamage;
        isCritical = _isCritical;
    }

    public override void Destroy()
    {
        theRB.velocity = Vector2.zero;
        theColl.enabled = false;
        anim.SetTrigger("touch");

    }

    public override void TakeDamage(Collider2D collision, bool? isCrit = null)
    {
        base.TakeDamage(collision, isCritical);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Ground")))
        {
            Destroy();
        }
    }
}
