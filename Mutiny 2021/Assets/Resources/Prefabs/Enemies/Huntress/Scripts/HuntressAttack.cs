using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuntressAttack : EnemyAttack
{
    [SerializeField] private GameObject GOSpear;

    public override int SetIndexAttack()
    {
        return 0;
    }

    private void ThrowSpear()
    {
        GameObject spear = Instantiate(GOSpear, arrAttackPoint[1].position, transform.rotation);
        spear.GetComponent<SpearHuntress>().Setup(theBS.attackDamage.GetValue(), this);
    }
}
