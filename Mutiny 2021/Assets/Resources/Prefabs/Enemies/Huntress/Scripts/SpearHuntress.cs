using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class SpearHuntress : Skill
{

    protected override void Start()
    {
        base.Start();
        theRB.velocity = new Vector2(transform.rotation.y == 0 ? speed : -speed, 0f);
        Destroy(gameObject, 5f);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D (collision);
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Ground")))
        {
            Destroy();
        }
    }

    public override void Destroy()
    {
        Destroy(gameObject);
    }
}
