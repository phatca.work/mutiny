using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero_KnightAttack : EnemyAttack
{
    public override int SetIndexAttack()
    {
        return 0;
    }

    private void TakeHit()
    {
        GetComponent<Rigidbody2D>().MovePosition(transform.position + new Vector3(transform.rotation.y == 0 ? 2f : -2f, 0f));
    }
}
