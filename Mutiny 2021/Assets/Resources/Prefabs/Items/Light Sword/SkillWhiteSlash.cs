using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillWhiteSlash : Skill
{
    private bool isReady;

    protected override void Start()
    {
        base.Start();
        theRB.velocity = new Vector2(originOfDamage.transform.rotation.y == 0 ? speed : -speed, 0f);
    }

    public override void Destroy()
    {
        Destroy(gameObject);
    }

    public override void TakeDamage(Collider2D collision, bool? isCrit = null)
    {
        if(isReady)
        {
            base.TakeDamage(collision);
        }
    }

    private void SetReady() => isReady = true;
}
