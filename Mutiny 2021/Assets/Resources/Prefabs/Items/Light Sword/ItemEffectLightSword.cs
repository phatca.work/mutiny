using UnityEngine.Events;
using UnityEngine;

public class ItemEffectLightSword : ItemEffect
{
    private Attacker theAttack;
    private BaseStats theBS;

    private void Start()
    {
        EventController.Instance.EventPlayerTakeHit.AddListener(Effect);
        theAttack = GetComponentInParent<Attacker>();
        theBS = GetComponentInParent<BaseStats>();
    }

    public override void Effect()
    {
        GameObject _GO = Instantiate(Resources.Load("Prefabs/Items/Light Sword/PF White Slash"), theAttack.arrAttackPoint[theAttack._attackIndex].position, transform.parent.rotation) as GameObject;
        _GO.GetComponent<Skill>().Setup(theBS.attackDamage.GetValue(), theAttack);
        Destroy(_GO, 1f);
    }

    public override void DestroyEffect()
    {
        EventController.Instance.EventPlayerTakeHit.RemoveListener(Effect);
    }
}
