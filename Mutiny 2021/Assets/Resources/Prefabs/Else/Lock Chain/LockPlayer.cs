using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPlayer : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject LockChain;
    [SerializeField] private bool isUnlocked;

    private Animator anim;

    private PlayerHealth thePH;
    private PlayerAttack thePA;
    private PlayerMovement thePM;
    private StateMachine theSM;
    private Unit theUnit;

    private void Start()
    {
        if(!isUnlocked)
        {
            anim = LockChain.GetComponent<Animator>();
            LockChain.SetActive(true);
        }
        
        ChangeBody(player);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightAlt))
        {
            Unlock();
        }
    }


    private void Unlock()
    {
        anim.SetTrigger("Unlock");
        isUnlocked = true;
    }


    private void ChangeBody(GameObject _GO)
    {
        thePH = _GO.GetComponent<PlayerHealth>();
        thePA = _GO.GetComponent<PlayerAttack>();
        thePM = _GO.GetComponent<PlayerMovement>();
        theUnit = _GO.GetComponent<Unit>();
        theSM = _GO.GetComponent<StateMachine>();
        _GO.transform.parent.SetParent(transform);
    }

    private void Selected()
    {
        thePH.healthBar.gameObject.SetActive(false);
        thePH.enabled = false;
        thePA.enabled = false;
        thePM.enabled = false;
        theSM.enabled = false;
        theUnit.enabled = false;
    }

    private void UnSelected(Transform _trans)
    {
        _trans.GetComponent<Animator>().SetFloat("moveSpeed", 0);
        _trans.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        _trans.position = new Vector2(transform.position.x, _trans.position.y);
        _trans.GetComponent<PlayerHealth>().healthBar.gameObject.SetActive(false);
        _trans.GetComponent<PlayerHealth>().enabled = false;
        _trans.GetComponent<PlayerAttack>().enabled = false;
        _trans.GetComponent<PlayerMovement>().enabled = false;
        _trans.GetComponent<StateMachine>().enabled = false;
        _trans.GetComponent<Unit>().enabled = false;

        thePH.healthBar.gameObject.SetActive(true);
        thePH.enabled = true;
        thePA.enabled = true;
        thePM.enabled = true;
        theSM.enabled = true;
        theUnit.enabled = true;
        player.transform.parent.SetParent(null);

        InputController.Instance.unitControl = theUnit;
        player = _trans.gameObject;
        ChangeBody(player);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
        {
            if (Input.GetKey(InputController.Instance.keyPickupItem))
            {
                if (isUnlocked)
                {
                    UnSelected(collision.transform);
                }
            }
        }
    }
}
