using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkAttack : PlayerAttack
{
    public override void Attack()
    {
        if (theSM.CurrentState.GetType() == typeof(IdleCombatState))
        {
            theSM.SetNextState(new MonkGroundComboState1());
        }
    }
}
