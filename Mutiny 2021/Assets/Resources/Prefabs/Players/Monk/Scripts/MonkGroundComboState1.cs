using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkGroundComboState1 : MeleeBaseState
{
    public override void OnEnter(StateMachine _stateMachine)
    {
        base.OnEnter(_stateMachine);
        shouldCombo = false;
        //Attack
        attackIndex = 1;
        duration = 0.45f;
        anim.SetTrigger("Attack" + attackIndex);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (fixedTime >= duration)
        {
            if (shouldCombo)
            {
                theSM.SetNextState(new MonkGroundComboState2());
            }
            else
            {
                theSM.SetNextStateToMain();
            }
        }
    }
}
