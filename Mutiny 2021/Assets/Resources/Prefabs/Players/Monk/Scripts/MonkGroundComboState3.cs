using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkGroundComboState3 : MeleeBaseState
{
    public override void OnEnter(StateMachine _stateMachine)
    {
        base.OnEnter(_stateMachine);
        shouldCombo = false;
        //Attack
        attackIndex = 3;
        duration = 0.45f;
        anim.SetTrigger("Attack" + attackIndex);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (fixedTime >= duration)
        {
            theSM.SetNextStateToMain();
        }
    }
}
