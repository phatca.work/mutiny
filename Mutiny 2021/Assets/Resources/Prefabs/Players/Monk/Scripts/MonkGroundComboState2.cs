using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkGroundComboState2 : MeleeBaseState
{
    public override void OnEnter(StateMachine _stateMachine)
    {
        base.OnEnter(_stateMachine);
        shouldCombo = false;
        //Attack
        attackIndex = 2;
        duration = 0.54f;
        if (thePM.isGrounded)
        {
            theRB.velocity = new Vector2(theRB.transform.rotation.y == 0 ? 2f : -2f, 0);
        }
        anim.SetTrigger("Attack" + attackIndex);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (fixedTime >= duration)
        {
            if (shouldCombo)
            {
                theSM.SetNextState(new MonkGroundComboState3());
            }
            else
            {
                theSM.SetNextStateToMain();
            }
        }
    }
}
