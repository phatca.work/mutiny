using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class WarriorAttack : PlayerAttack
{
    public override void Attack()
    {
        if (theSM.CurrentState.GetType() == typeof(IdleCombatState))
        {
            theSM.SetNextState(new WarriorGroundComboState1());
        }
    }
}
