using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorGroundComboState1: MeleeBaseState
{
    public override void OnEnter(StateMachine _stateMachine)
    {
        base.OnEnter(_stateMachine);
        shouldCombo = false;
        //Attack
        attackIndex = 1;
        duration = 0.5f;
        anim.SetTrigger("Attack" + attackIndex);
        Debug.Log("Player Attack " + attackIndex + " Fired!");
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (fixedTime >= duration)
        {
            if (shouldCombo)
            {
                theSM.SetNextState(new WarriorGroundComboState2());
            }
            else
            {
                theSM.SetNextStateToMain();
            }
        }
    }
}