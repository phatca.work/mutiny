using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Martial2GroundComboState1 : MeleeBaseState
{
    public override void OnEnter(StateMachine _stateMachine)
    {
        base.OnEnter(_stateMachine);
        shouldCombo = false;
        //Attack
        attackIndex = 1;
        duration = 0.4f;
        if (thePM.isGrounded)
        {
            theRB.velocity = new Vector2(theRB.transform.rotation.y == 0 ? 1.5f : -1.5f, 0);
        }
        anim.SetTrigger("Attack" + attackIndex);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (fixedTime >= duration)
        {
            if (shouldCombo)
            {
                theSM.SetNextState(new Martial2GroundComboState2());
            }
            else
            {
                theSM.SetNextStateToMain();
            }
        }
    }
}
