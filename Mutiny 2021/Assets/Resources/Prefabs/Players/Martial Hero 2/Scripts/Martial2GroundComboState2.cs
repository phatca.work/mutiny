using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Martial2GroundComboState2 : MeleeBaseState
{
    public override void OnEnter(StateMachine _stateMachine)
    {
        base.OnEnter(_stateMachine);

        //Attack
        attackIndex = 2;
        duration = 0.4f;
        anim.SetTrigger("Attack" + attackIndex);
    }

    public override void OnUpdate()
    {
        base.OnUpdate();

        if (fixedTime >= duration)
        {
            theSM.SetNextStateToMain();
        }
    }
}
