using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Martial2Attack : PlayerAttack
{
    public override void Attack()
    {
        if (theSM.CurrentState.GetType() == typeof(IdleCombatState))
        {
            theSM.SetNextState(new Martial2GroundComboState1());
        }
    }
}
