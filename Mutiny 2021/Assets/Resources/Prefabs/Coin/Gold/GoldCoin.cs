using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldCoin : Coin
{
    protected override void RemoveCoin()
    {
        base.RemoveCoin();
        PoolGoldCoin.instance.RemoveObjectToPool(gameObject);
    }
}
