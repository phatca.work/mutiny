using UnityEngine;

public class PoolAPileOfMoney : Pooler
{
    public static PoolAPileOfMoney instance;
    protected override void Awake()
    {
        base.Awake();
        instance = this;
    }

    public override void GetObjectFromPool(Vector3 position, params object[] arrObject)
    {
        var GO = GetObject(position);
        GO.GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-2f, 2f), Random.Range(7f, 10f));
        GO.GetComponent<Coin>().coin = (int)arrObject[0];
    }

    public override void RemoveObjectToPool(GameObject theGO, params object[] arrObject)
    {
        theGO.TryGetComponent(out Coin _coin);
        _coin.Reset();
        _coin.start = false;
        _coin.stop = true;
        ReturnObject(theGO);
    }
}
