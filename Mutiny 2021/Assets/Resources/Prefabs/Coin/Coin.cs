using UnityEngine;

public abstract class Coin : MonoBehaviour
{
    private Rigidbody2D theRB;
    private SpriteRenderer theSR;

    public int coin;

    public bool start = false;
    public bool stop = true;


    // Start is called before the first frame update
    void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
        theSR = GetComponent<SpriteRenderer>();
    }

    public void Reset()
    {
        theSR.color = new Color(1f, 1f, 1f, 1f);
        GetComponent<Collider2D>().enabled = true;
    }

    // Update is called once per frame
    [System.Obsolete]
    void FixedUpdate()
    {
        if (!gameObject.active) return;
        if (theRB.velocity == Vector2.zero)
        {
            start = true;
        }
        if (start)
        {
            StartFollow();
        }
    }

    private void StartFollow()
    {
        GetComponent<Collider2D>().enabled = false;
        Vector2 _direction = InputController.Instance.unitControl.transform.position - transform.position;
        if (stop)
        {
            float speed = Mathf.Abs(_direction.magnitude) > 10f ? _direction.magnitude * 2f : 10f;
            theRB.velocity = new Vector2(_direction.x, _direction.y).normalized * speed;
            if (Mathf.Abs(_direction.magnitude) < 0.1f)
            {
                stop = false;
                theRB.velocity = Vector2.zero;
            }
        }
        else
        {
            theSR.color = new Color(theSR.color.r, theSR.color.b, theSR.color.g, theSR.color.a - 0.05f);
            if (theSR.color.a < 0.1f)
            {
                RemoveCoin();
            }
        }
        
    }

    protected virtual void RemoveCoin()
    {
        EventController.Instance.EventChangeCoin.Invoke(coin);
    }
}
