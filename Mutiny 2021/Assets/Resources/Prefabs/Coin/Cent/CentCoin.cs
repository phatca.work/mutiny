public class CentCoin : Coin
{
    protected override void RemoveCoin()
    {
        base.RemoveCoin();
        PoolCentCoin.instance.RemoveObjectToPool(gameObject);
    }
}
