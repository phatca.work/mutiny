using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class UICoin : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textCoin;
    private int currentCoin;
    private int changeCoin;
    private int speed;

    private void FixedUpdate()
    {
        if (currentCoin != changeCoin)
        {
            int number = Mathf.Abs(Mathf.Abs(currentCoin) - Mathf.Abs(changeCoin));
            if (number >= 500) speed = 50;
            else if (number >= 200) speed = 20;
            else if (number >= 100) speed = 10;
            else if (number >= 50) speed = 5;
            else if (number >= 20) speed = 2;
            else speed = 1;

            currentCoin += changeCoin > 0 ? speed : -speed;
            _textCoin.text = currentCoin.ToString();

        }


    }

    private void Start()
    {
        EventController.Instance.EventChangeCoin.AddListener(param => ChangeCoinUI(param));
    }

    private void ChangeCoinUI(int _coinNumber)
    {
        changeCoin += _coinNumber;

    }
}
