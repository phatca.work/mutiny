using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static UnityEditor.Progress;

public class ItemController : Singleton<ItemController>
{
    public List<Item> listItemCollected = new();
    private GameObject GOListItemOfPlayer;
   
    private void Start()
    {
        GOListItemOfPlayer = GameObject.FindGameObjectWithTag("ListItemOfPlayer");
    }

    public void PickupItem(ItemObject itemTouched)
    {
        Debug.Log("Collected item: " + itemTouched.name);
        if (CheckExistItem(itemTouched.item))
        {
            Debug.Log(itemTouched.item.itemEffect);
            var itemEff = GOListItemOfPlayer.AddComponent(itemTouched.GetComponent<ItemEffect>().GetType());
        }
        listItemCollected.Add(itemTouched.item);
        EventDispatcher.Instance.PostEvent(EventID.PickUpItem, itemTouched.item);
        itemTouched.Destroy();
    }

    public void DestroyItem(Item _item)
    {
        foreach (var item in listItemCollected)
        {
            if(item.ID == _item.ID)
            {
                _item.itemEffect.DestroyEffect();
            }
        }
        listItemCollected.Remove(_item);
        if (CheckExistItem(_item))
        {
            Destroy(GOListItemOfPlayer.GetComponent(_item.itemEffect.GetType()));
        }
    }

    private bool CheckExistItem(Item _item)
    {
        foreach (var item in listItemCollected)
        {
            if(item.ID == _item.ID) return false;
        }
        return true;
    }

    #region itemUI

    #endregion
}

public enum RateItem
{
    OneStar,
    TwoStar,
    ThreeStar
}
