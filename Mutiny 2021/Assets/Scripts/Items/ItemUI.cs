using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemUI : MonoBehaviour
{
    [SerializeField] private Image artWork;
    public Item ItemInSlot;
    public bool isHaveItem;

    public void AddItem(Item _item)
    {
        ItemInSlot = _item;
        artWork.sprite = _item.artWork;
        isHaveItem = true;
    }

    public void RemoveItem() {
        ItemInSlot = null;
        artWork.sprite = null;
        isHaveItem = false;
    }
}
