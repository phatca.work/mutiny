using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemObject : MonoBehaviour
{
    public Item item;

    private void Start()
    {
        item.itemEffect = GetComponent<ItemEffect>();
    }

    public void Touching()
    {
        throw new System.NotImplementedException();
    }

    public virtual void Destroy()
    {
        Instantiate(Resources.Load("Prefabs/Items/DestroyItemEffect/PF Effect Destroy Item"), transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
}
