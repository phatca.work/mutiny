using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Item")]
public class Item : ScriptableObject
{
    public int ID;
    public string ItemName;
    public RateItem Rate;
    public string description;
    public Sprite artWork;
    public ItemEffect itemEffect;
}
