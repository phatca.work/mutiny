using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemEffect : MonoBehaviour
{
    public int ID;

    public abstract void Effect();

    public abstract void DestroyEffect();
}
