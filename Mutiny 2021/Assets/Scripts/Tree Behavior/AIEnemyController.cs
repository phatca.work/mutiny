using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;

public class AIEnemyController : MonoBehaviour
{
    private Tree _tree;
    public int index;
    private EnemyAttack theEA;
    private EnemyMovement theEM;
    private Victim theIV;

    private void Start()
    {
        theEA = GetComponent<EnemyAttack>();
        theEM = GetComponent<EnemyMovement>();
        theIV = GetComponent<Victim>();
        _tree = new()
        {
            rootNode = new Selector(
            new Leaf(this.CheckTimeHurt),

             new Sequence(
                 new Leaf(this.CheckAreaAttack),
                 new RepeatUntilSuccess(
                     new Leaf(this.Attack)
                     )
                 ),

            new Sequence(
                new Succeeder(
                    new Leaf(this.Moving)
                    )
                ,

                new Selector(
                    new Leaf(this.Detect),
                    new Leaf(this.CheckTimeBreak)
                    )
                )
            )
        };

        /*#region root of Hurt
        Leaf leaf_Hurt = new();
        leaf_Hurt.SetFunction(CheckTimeHurt);
        rootNode.AddChild(leaf_Hurt);
        #endregion

        #region root of Move   
        Sequence selec_Move = new();
        rootNode.AddChild(selec_Move);*/

        /*Selector selec_TypeMove = new();
        selec_Move.AddChild(selec_TypeMove);

        Leaf leaf_TimeBreak = new();
        leaf_TimeBreak.SetFunction(CheckTimeBreak);
        selec_TypeMove.AddChild(leaf_TimeBreak);*/

        /*Succeeder succe_Moving = new();
        selec_Move.AddChild(succe_Moving);

        Leaf leaf_Moving = new();
        leaf_Moving.SetFunction(Moving);
        succe_Moving.child = leaf_Moving;
        #endregion*/
    }

    private void Update()
    {
        _tree.Tick();
    }

    public NodeState Attack()
    {
        /*if (!theEA.InAttackAnimation())
        {
            return NodeState.Success;
        }*/
        return NodeState.Running;
    }

    public NodeState CheckAreaAttack()
    {
        /*if (theEA.CheckEnemyInArea())
        {
            return NodeState.Success;
        }*/
        return NodeState.Failure;
    }

    public NodeState Detect()
    {
        if (theEM.Detect()) return NodeState.Success;
        return NodeState.Failure;
    }

    public NodeState CheckTimeBreak()
    {
        if (theEM.HaveTimeBreak())
        {
            return NodeState.Success;
        }
        return NodeState.Failure;
    }

    public NodeState CheckTimeHurt()
    {
        //if (!theIV.CheckTimeHurt()) return NodeState.Running;
        return NodeState.Failure;
    }

    public NodeState Moving()
    {
        theEM.Move();
        return NodeState.Success;
    }
}
