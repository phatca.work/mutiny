using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TaskPatrol : Node
{
    private readonly EnemyMovement theEM;
    public TaskPatrol(Transform transform)
    {
        theEM = transform.GetComponent<EnemyMovement>();
    }

    public override NodeState Run()
    {
        theEM.Move();
        return state = NodeState.Running;
    }
}
