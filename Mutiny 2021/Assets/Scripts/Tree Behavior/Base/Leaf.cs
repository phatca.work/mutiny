public class Leaf : Node
{
    public Leaf() { }

    public Leaf(LeafNodeDelegate func)
    {
        this.function = func;
    }

    public delegate NodeState LeafNodeDelegate();

    public LeafNodeDelegate function;

    public void SetFunction(LeafNodeDelegate func) => this.function = func;

    public override NodeState Run() => function();
}