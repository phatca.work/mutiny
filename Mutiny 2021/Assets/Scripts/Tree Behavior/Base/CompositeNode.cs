using System.Collections.Generic;

public abstract class CompositeNode : Node
{
    public List<Node> listChildren = new();

    public CompositeNode(params Node[] arrNode)
    {
        foreach (var node in arrNode)
        {
            AddChild(node);
        }
    }

    public void AddChild(Node child)
    {
        listChildren.Add(child);
    }

    public override NodeState Run()
    {
        throw new System.NotImplementedException();
    }
}

public class Selector : CompositeNode
{
    public Selector(params Node[] arrNode) : base(arrNode) { }
    public override NodeState Run()
    {
        if (listChildren.Count == 0) return NodeState.Failure;

        for (int i = 0; i < listChildren.Count; i++)
        {
            NodeState state = listChildren[i].Run();
            if (state != NodeState.Failure) return state;
        }

        return NodeState.Failure;
    }
}


public class Sequence : CompositeNode
{
    public Sequence(params Node[] arrNode) : base(arrNode) { }

    public override NodeState Run()
    {
        if (listChildren.Count == 0) return NodeState.Failure;
        for (int i = 0; i < listChildren.Count; i++)
        {
            NodeState state = listChildren[i].Run();
            if (state != NodeState.Success) return state;
        }
        return NodeState.Success;
    }
}
