public abstract class Decorator : Node
{
    public Node child;

    public Decorator(Node child)
    {
        this.child = child;
    }

    public abstract NodeState ProcessResult(NodeState childState);

    public override NodeState Run()
    {
        NodeState childState = child.Run();
        return ProcessResult(childState);
    }
}

public class Inverter : Decorator
{
    public Inverter(Node child) : base(child) {}
    public override NodeState ProcessResult(NodeState childState)
    {
        if (childState == NodeState.Success) return NodeState.Failure;
        if (childState == NodeState.Failure) return NodeState.Success;
        return NodeState.Running;
    }
}

public class Succeeder : Decorator
{
    public Succeeder(Node child) : base(child) { }
    public override NodeState ProcessResult(NodeState childState)
    {
        return NodeState.Success;
    }
}

public class RepeatUntilSuccess: Decorator
{
    public RepeatUntilSuccess(Node child) : base(child) { }

    public override NodeState ProcessResult(NodeState childState)
    {
        while(true)
        {
            if(child.Run() == NodeState.Success)
            {
                return NodeState.Success;
            }
        }
    }
}

public class RepeatUntilFailure : Decorator
{
    public RepeatUntilFailure(Node child) : base(child) { }

    public override NodeState ProcessResult(NodeState childState)
    {
        while (true)
        {
            if (childState == NodeState.Failure)
            {
                return NodeState.Success;
            }
        }
    }
}
