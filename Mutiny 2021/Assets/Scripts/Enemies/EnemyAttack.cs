using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class EnemyAttack : Attacker
{
    [SerializeField] protected Vector2 areaAttack;
    
    [SerializeField] protected float _timeBreakAfterAttack;
    [SerializeField] protected float _waitTimeBreakAfterAttack;

    protected bool RestAfterAttack()
    {
        if(_waitTimeBreakAfterAttack > 0.1f)
        {
            _waitTimeBreakAfterAttack -= Time.deltaTime;
            anim.SetFloat("moveSpeed", 0f);
            return true;
        }
        return false;
    }

    public virtual bool CheckAttack()
    {
        if (RestAfterAttack()) return true;
        if (anim.GetBool("isAttack")) return true;
        Collider2D enemy = Physics2D.OverlapBox(arrAttackPoint[0].position, areaAttack, 0f, enemyLayers);    
        if (enemy == null || enemy.GetComponent<BaseStats>().died)
        {
            return false;
        }
        StartAnimAttack();
        return true;
    }

    protected virtual void StartAnimAttack()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        anim.SetTrigger("attack" + SetIndexAttack());
        anim.SetBool("isAttack", true);
    }

    protected virtual void DoneAttackAnim()
    {
        _waitTimeBreakAfterAttack = _timeBreakAfterAttack;
        collidersDamaged.Clear();   
        anim.SetBool("isAttack", false);
    }

    public abstract int SetIndexAttack();

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        if (arrAttackPoint.Length == 0 || arrAttackRange.Length == 0) return;
        Gizmos.DrawWireCube(arrAttackPoint[0].position, areaAttack);
    }
}
