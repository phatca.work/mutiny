using UnityEngine;

public class EnemyControllerNoMove : MonoBehaviour
{
    private BaseStats theBS;
    private EnemyAttack theEA;
    private Victim theEH;

    private void Start()
    {
        theBS = GetComponent<BaseStats>();
        theEA = GetComponent<EnemyAttack>();
        theEH = GetComponent<Victim>();
    }

    // Update is called once per frame
    void Update()
    {
        if (theBS.died) return;
        if (theEH.hurtingTime > 0) return;
        if (theEA.CheckAttack()) return;
    }
}
