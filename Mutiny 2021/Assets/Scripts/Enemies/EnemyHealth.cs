public class EnemyHealth : Victim
{
    protected override void Die()
    {
        base.Die();
        EventController.Instance.EventEnemyDie.Invoke(theBS);
    }
}
