using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private BaseStats theBS;
    private EnemyAttack theEA;
    private EnemyMovement theEM;
    private Victim theEH;

    private void Start()
    {
        theBS = GetComponent<BaseStats>();
        theEA = GetComponent<EnemyAttack>();
        theEM = GetComponent<EnemyMovement>();
        theEH = GetComponent<Victim>();
    }

    // Update is called once per frame
    void Update()
    {
        if (theBS.died) return;
        if (theEH.hurtingTime > 0) return;
        if (theEM.CheckGrounded()) return;
        if (theEA.CheckAttack()) return;
        theEM.Move();
    }
}
