using UnityEngine;

public class EnemyMovement : Movement
{
    protected SpriteRenderer theSR;

    private bool movingRight;

    [Header("Move to Right or Left point")]
    [SerializeField] private bool turnOnRightLeftPoint;
    [SerializeField] private Transform leftPoint;
    [SerializeField] private Transform rightPoint;

    [Header("Turnback if touching")]
    [SerializeField] private bool turnOnTurnBack;
    public Transform facePoint;
    public LayerMask whichIsTouched;

    [Header("Have time breaks")]
    [SerializeField] private bool turnOnTimeBreaks;
    [SerializeField] private float setTimeMove;
    [SerializeField] private float setTimeBreaks;
    [SerializeField] private float timeBreaks;
    [SerializeField] private float timeMove;

    [Header("Detect the target")]
    [SerializeField] private bool turnOnDetect;
    [SerializeField] LayerMask whichIsTarget;
    [SerializeField] private Vector2 areaDetect;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        if (!turnOnRightLeftPoint && !turnOnTurnBack)
            Debug.LogWarning("WARNING!!! You need to check *turnOnRightLeftPoint* or *turnOnTurnBack* to " + name + " can move");
        theSR = GetComponent<SpriteRenderer>();
        movingRight = true;
    }

    public bool Detect()
    {
        if (!turnOnDetect) return false;
        Collider2D deteced = Physics2D.OverlapBox(transform.position, areaDetect, 0f, whichIsTarget);
        if (deteced == null) return false;

        if (transform.position.x > deteced.transform.position.x + 0.1f)
        {
            movingRight = true;
        }
        else if (transform.position.x < deteced.transform.position.x - 0.1f)
        {
            movingRight = false;
        }
        else
        {
            theRB.velocity = new Vector2(0f, 0f);
            anim.SetFloat("moveSpeed", 0f);
            return false;
        }
        return true;
    }

    public bool HaveTimeBreak()
    {
        if (!turnOnTimeBreaks) return true;
        if (timeBreaks < 0)
        {
            timeMove = setTimeMove;
            timeBreaks = setTimeBreaks;
        }
        else if (timeMove >= 0)
        {
            timeMove -= Time.deltaTime;
            return true;
        }
        else if (timeBreaks >= 0)
        {
            timeBreaks -= Time.deltaTime;
            anim.SetFloat("moveSpeed", 0f);
            return false;
        }
        anim.SetFloat("moveSpeed", Mathf.Abs(theRB.velocity.x));
        return false;
    }

    private void RightLeftPoint()
    {
        if (!turnOnRightLeftPoint) return;
        if (transform.position.x < leftPoint.position.x)
        {
            movingRight = false;
        }

        if (transform.position.x > rightPoint.position.x)
        {
            movingRight = true;
        }
    }

    private void TurnBackWhenTouched()
    {
        if (!turnOnTurnBack) return;
        bool isWall = Physics2D.OverlapCircle(facePoint.position, .2f, whichIsTouched);
        if (isWall)
        {
            movingRight = !movingRight;
        }
    }

    private void BaseMove()
    {
        theRB.velocity = new Vector2(movingRight ? -theBS.moveSpeed.GetValue() : theBS.moveSpeed.GetValue(), 0f);
        transform.rotation = Quaternion.Euler(0, movingRight ? 180 : 0, 0);
        anim.SetFloat("moveSpeed", Mathf.Abs(theRB.velocity.x));
        //research how to get a state in animator
    }

    public void Move()
    {
        anim.SetFloat("moveSpeed", Mathf.Abs(theRB.velocity.x));
        if (!HaveTimeBreak()) return;
        TurnBackWhenTouched();
        RightLeftPoint();
        BaseMove();
        Detect();
    }

    private void OnDrawGizmosSelected()
    {
        if (!turnOnDetect && !areaDetect.Equals(Vector2.zero)) return;
        Gizmos.DrawWireCube(transform.position, areaDetect);
    }
}
