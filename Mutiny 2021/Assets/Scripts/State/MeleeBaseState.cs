using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MeleeBaseState : State
{
    protected PlayerAttack thePA;
    protected Rigidbody2D theRB;
    protected PlayerMovement thePM;

    // How long this state should be active for before moving on
    public float duration;
    // Cached animator component
    protected Animator anim;
    // bool to check whether or not the next attack in the sequence should be played or not
    protected bool shouldCombo;
    // The attack index in the sequence of attacks
    protected int attackIndex;
    // Input buffer Timer
    protected float AttackPressedTimer = 0.01f;

    public override void OnEnter(StateMachine _stateMachine)
    {
        base.OnEnter(_stateMachine);
        anim = GetComponent<Animator>();
        thePA = GetComponent<PlayerAttack>();
        theRB = GetComponent<Rigidbody2D>();
        thePM = GetComponent<PlayerMovement>();
        if(thePM.isGrounded)
        {
            theRB.velocity = Vector2.zero;
        }
        InputController.Instance.stopWhenAttack = true;
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        AttackPressedTimer -= Time.deltaTime;

        if (anim.GetFloat("Weapon.Active") > 0f)
        {
            thePA.Attack(attackIndex - 1);
        }

        if (Input.GetKeyDown(InputController.Instance.keyAttack))
        {
            AttackPressedTimer = 0.01f;
        }

        if (anim.GetFloat("AttackWindow.Open") > 0f && AttackPressedTimer > 0)
        {
            shouldCombo = true;
        }
    }

    public override void OnExit()
    {
        base.OnExit();
        thePA.collidersDamaged.Clear();
        InputController.Instance.stopWhenAttack = false;
    }
}
