using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Unity.Burst.Intrinsics;
using UnityEditor.PackageManager;
using System.Diagnostics;
using Debug = UnityEngine.Debug;




public class EventDispatcher : Singleton<EventDispatcher>
{
    #region Fields
    /// Store all "listener"
    private Dictionary<EventID, Action<object>> listeners = new();
    #endregion

    private void Update()
    {
        if(listeners.ContainsKey(EventID.EventPlayerTakeHit))
        Debug.Log(listeners[EventID.EventPlayerTakeHit]);
    }

    #region Add Listeners, Post events, Remove listener

    /// <summary>
    /// Register to listen for eventID
    /// </summary>
    /// <param name="eventID">EventID that object want to listen</param>
    /// <param name="callback">Callback will be invoked when this eventID be raised</para	m>
    public void RegisterListener(EventID eventID, Action<object> callback)
    {
        // checking params
        Common.Assert(callback != null, "AddListener, event {0}, callback = null !!", eventID.ToString());
        Common.Assert(eventID != EventID.None, "RegisterListener, event = None !!");

        // check if listener exist in distionary
        if (listeners.ContainsKey(eventID))
        {
            // add callback to our collection
            listeners[eventID] += callback;
        }
        else
        {
            // add new key-value pair
            listeners.Add(eventID, null);
            listeners[eventID] += callback;
        }
    }

    /// <summary>
    /// Posts the event. This will notify all listener that register for this event
    /// </summary>
    /// <param name="eventID">EventID.</param>
    /// <param name="sender">Sender, in some case, the Listener will need to know who send this message.</param>
    /// <param name="param">Parameter. Can be anything (struct, class ...), Listener will make a cast to get the data</param>
    public void PostEvent(EventID eventID, object param = null)
    {
        if (!listeners.ContainsKey(eventID))
        {
            Common.Log("No listeners for this event : {0}", eventID);
            return;
        }

        // posting event
        var callbacks = listeners[eventID];
        // if there's no listener remain, then do nothing
        if (callbacks != null)
        {
            callbacks(param);
        }
        else
        {
            Common.Log("PostEvent {0}, but no listener remain, Remove this key", eventID);
            listeners.Remove(eventID);
        }
    }

    /// <summary>
    /// Removes the listener. Use to Unregister listener
    /// </summary>
    /// <param name="eventID">EventID.</param>
    /// <param name="callback">Callback.</param>
    public void RemoveListener(EventID eventID, Action<object> callback)
    {
        Debug.Log("Removed: " + eventID.ToString() + ", " + callback);
        // checking params
        Common.Assert(callback != null, "RemoveListener, event {0}, callback = null !!", eventID.ToString());
        Common.Assert(eventID != EventID.None, "AddListener, event = None !!");

        if (listeners.ContainsKey(eventID))
        {
            listeners[eventID] -= callback;
        }
        else
        {
            Common.Warning(false, "RemoveListener, not found key : " + eventID);
        }
    }

    /// <summary>
    /// Clears all the listener.
    /// </summary>
    public void ClearAllListener()
    {
        listeners.Clear();
    }
    #endregion
}

public class Common
{
    //-----------------------------------
    //--------------------- Log , warning, 

    [Conditional("DEBUG")]
    public static void Log(object message)
    {
        Debug.Log(message);
    }

    [Conditional("DEBUG")]
    public static void Log(string format, params object[] args)
    {
        Debug.Log(string.Format(format, args));
    }

    [Conditional("DEBUG")]
    public static void LogWarning(object message, UnityEngine.Object context)
    {
        Debug.LogWarning(message, context);
    }

    [Conditional("DEBUG")]
    public static void LogWarning(UnityEngine.Object context, string format, params object[] args)
    {
        Debug.LogWarning(string.Format(format, args), context);
    }



    [Conditional("DEBUG")]
    public static void Warning(bool condition, object message)
    {
        if (!condition) Debug.LogWarning(message);
    }

    [Conditional("DEBUG")]
    public static void Warning(bool condition, object message, UnityEngine.Object context)
    {
        if (!condition) Debug.LogWarning(message, context);
    }

    [Conditional("DEBUG")]
    public static void Warning(bool condition, UnityEngine.Object context, string format, params object[] args)
    {
        if (!condition) Debug.LogWarning(string.Format(format, args), context);
    }


    //---------------------------------------------
    //------------- Assert ------------------------

    /// Thown an exception if condition = false
    [Conditional("ASSERT")]
    public static void Assert(bool condition)
    {
        if (!condition) throw new UnityException();
    }

    /// Thown an exception if condition = false, show message on console's log
    [Conditional("ASSERT")]
    public static void Assert(bool condition, string message)
    {
        if (!condition) throw new UnityException(message);
    }

    /// Thown an exception if condition = false, show message on console's log
    [Conditional("ASSERT")]
    public static void Assert(bool condition, string format, params object[] args)
    {
        if (!condition) throw new UnityException(string.Format(format, args));
    }
}


