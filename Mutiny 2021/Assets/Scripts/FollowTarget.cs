using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float distanceX, distanceY;

    private void FixedUpdate()
    {
        transform.position = new Vector3(target.position.x + distanceX, target.position.y + distanceY, 0f);
    }
}
