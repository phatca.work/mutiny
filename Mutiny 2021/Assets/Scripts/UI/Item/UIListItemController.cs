using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Progress;

public class UIListItemController : Singleton<UIListItemController>
{
    public ItemUI[,] matrixListItemUI = new ItemUI[3, 3];
    [SerializeField] private Transform listItem;
    [SerializeField] private Slider fillDestroy;
    [SerializeField] private GameObject GOListItem;
    private bool isOpen;

    private ItemUI slotSelected;
    private bool holdButtonItem;

    private int rowListItem;
    private int colListItem;

    void Start()
    {
        EventDispatcher.Instance.RegisterListener(EventID.PickUpItem, (param) => AddItemUI((Item)param));

        int count = 0;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                matrixListItemUI[i, j] = listItem.GetChild(count).GetComponent<ItemUI>();
                count++;
            }
        }
        SelectSlot(matrixListItemUI[0, 0]);
    }

    void Update()
    {
        MoveSlot();
        if (slotSelected.GetComponent<ItemUI>().isHaveItem)
        {
            InteractItem();
        }
        else
        {
            holdButtonItem = false;
        }
        if (!holdButtonItem)
            fillDestroy.value -= 0.005f;
    }

    private void MoveSlot()
    {
        bool change = false;
        if (Input.GetKeyDown(KeyCode.S))
        {
            colListItem++;
            if (colListItem == 3) colListItem = 0;
            change = true;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            colListItem--;
            if (colListItem == -1) colListItem = 2;
            change = true;
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            rowListItem++;
            if (rowListItem == 3) rowListItem = 0;
            change = true;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            rowListItem--;
            if (rowListItem == -1) rowListItem = 2;
            change = true;
        }

        if (change)
        {
            UnSelectSlot();
            SelectSlot(matrixListItemUI[colListItem, rowListItem]);
        }
    }

    private void SelectSlot(ItemUI _item)
    {
        Image _slot = _item.GetComponent<Image>();
        _slot.color = new Color32(9, 9, 217, 255);
        slotSelected = _item;
    }

    private void UnSelectSlot()
    {
        Image _slot = slotSelected.GetComponent<Image>();
        _slot.color = new Color32(217, 9, 9, 255);
        slotSelected = null;
    }

    private void AddItemUI(Item _item)
    {
        foreach (var item in matrixListItemUI)
        {
            if (item.TryGetComponent(out ItemUI _itemUI) && !_itemUI.isHaveItem)
            {
                _itemUI.AddItem(_item);
                break;
            }
        }
    }

    private void InteractItem()
    {
        if (Input.GetKey(InputController.Instance.keyPickupItem))
        {
            HoldButtonItem();
        }
        if (Input.GetKeyUp(InputController.Instance.keyPickupItem))
        {
            holdButtonItem = false;
        }
    }

    private void HoldButtonItem()
    {
        holdButtonItem = true;
        fillDestroy.value += 0.005f;
        if (fillDestroy.value == 1)
        {
            DestroyItemInListUI();
        }
    }

    private void DestroyItemInListUI()
    {
        ItemController.Instance.DestroyItem(slotSelected.ItemInSlot);
        slotSelected.RemoveItem();
        fillDestroy.value = 0;
    }

    private void OpenTableListItemUI()
    {
        isOpen = false;
        GOListItem.SetActive(true);
        Time.timeScale = 0f;
    }

    private void CloseTableListItemUI()
    {
        isOpen = true;
        GOListItem.SetActive(false);
        Time.timeScale = 1f;
    }

    public void InteractTableListItemUI()
    {
        if (isOpen)
        {
            OpenTableListItemUI();
        }
        else
        {
            CloseTableListItemUI();
        }
    }
}
