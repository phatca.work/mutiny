using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIInforItem : Singleton<UIInforItem>
{
    [SerializeField] private TextMeshProUGUI UI_ItemName;
    [SerializeField] private TextMeshProUGUI UI_ItemRate;
    [SerializeField] private TextMeshProUGUI UI_Description;
    [SerializeField] private Slider fillDestroy;

    [SerializeField] private GameObject PanelInforItem;
    private bool holdButtonItem;
    public ItemObject itemTouched;

    private void Update()
    {
        if (itemTouched != null)
        {
            InteractItem();
        }

        if (!holdButtonItem)
            fillDestroy.value -= (Time.deltaTime * 0.5f);
    }

    public void TouchItem(ItemObject _itemObject)
    {
        PanelInforItem.SetActive(true);
        itemTouched = _itemObject;
        SetUIItem(_itemObject.item);
    }
    public void UnTouchItem()
    {
        PanelInforItem.SetActive(false);
        itemTouched = null;
        fillDestroy.value = 0;
    }

    private void SetUIItem(Item item)
    {
        UI_ItemName.text = item.name;
        UI_ItemRate.text = item.Rate.ToString();
        UI_Description.text = item.description;
    }

    public void HoldButtonItem()
    {
        holdButtonItem = true;
        fillDestroy.value += (Time.deltaTime * 0.5f);
        if (fillDestroy.value == 1)
        {
            itemTouched.Destroy();
        }
    }

    public void UnHoldButtonItem()
    {
        holdButtonItem = false;
        if (fillDestroy.value < 1)
        {
            ItemController.Instance.PickupItem(itemTouched);
        }
    }

    public void InteractItem()
    {
        if (Input.GetKey(InputController.Instance.keyPickupItem))
        {
            /*ItemController.Instance.PickupItem(itemTouched);*/
            HoldButtonItem();
        }
        if (Input.GetKeyUp(InputController.Instance.keyPickupItem))
        {
            UnHoldButtonItem();
        }
    }
}
