using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    static T s_instance;
    public static T Instance
    {
        get
        {
            // instance not exist, then create new one
            if (s_instance == null)
            {
                // create new Gameobject, and add EventDispatcher component
                GameObject singletonObject = new();
                s_instance = singletonObject.AddComponent<T>();
                singletonObject.name = "Singleton - " + typeof(T).Name;
                Common.Log("Create singleton : {0}", singletonObject.name);
            }
            return s_instance;
        }
        private set { }
    }

    public static bool HasInstance()
    {
        return s_instance != null;
    }

    void Awake()
    {
        // check if there's another instance already exist in scene
        if (s_instance != null && s_instance.GetInstanceID() != this.GetInstanceID())
        {
            // Destroy this instances because already exist the singleton of EventsDispatcer
            Common.Log("An instance of " + typeof(T).Name + " already exist : <{1}>, So destroy this instance : <{2}>!!", s_instance.name, name);
            Destroy(gameObject);
        }
        else
        {
            // set instance
            s_instance = this as T;
        }
    }


    void OnDestroy()
    {
        // reset this static var to null if it's the singleton instance
        if (s_instance == this)
        {
            s_instance = null;
        }
    }
}