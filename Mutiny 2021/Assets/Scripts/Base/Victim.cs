using UnityEngine;

public abstract class Victim : MonoBehaviour
{
    private Animator anim;
    protected BaseStats theBS;
    protected Rigidbody2D theRB;
    public float hurtingTime;
    public HealthBar healthBar;

    public Material materialDeathEffect;
    private Material myMaterial;

    private float timeToDissolving = 1f;
    private bool isDissolving = false;


    public bool isInvicible;
    private float _timeInvincible;


    protected virtual void Start()
    {
        theBS = GetComponent<BaseStats>();
        theRB = GetComponent<Rigidbody2D>();
        myMaterial = new Material(materialDeathEffect);
        anim = GetComponent<Animator>();
        healthBar.SetMaxHealth(theBS.maxHP.GetValue());
    }

    protected virtual void Update()
    {
        CheckTimeHurt();
        Dissolving();
        if (_timeInvincible > 0) _timeInvincible -= Time.deltaTime;
    }

    public void CheckTimeHurt()
    {
        if (hurtingTime >= 0)
        {
            anim.SetFloat("hurt", hurtingTime);
            hurtingTime -= Time.deltaTime;
        }
    }

    public void Dissolving()
    {
        if (!isDissolving) return;
        timeToDissolving -= Time.deltaTime;
        if (timeToDissolving <= 0f)
        {
            timeToDissolving = 0f;
            isDissolving = false;
            gameObject.transform.parent.gameObject.SetActive(false);
        }
        myMaterial.SetFloat("_Fade", timeToDissolving);
        healthBar.DissolvingHealthBar(timeToDissolving);
    }

    protected virtual void TakeDamage(int _damage, bool _isCritical)
    {
        if (_isCritical) _damage *= 2;
        theBS.currentHP.AddValue(-(_damage));
        healthBar.SetHealth(theBS.currentHP.GetValue());
        PoolDmgPoint.instance.GetObjectFromPool(transform.position, _damage, _isCritical);
    }

    protected virtual void Die()
    {
        GetComponent<SpriteRenderer>().material = myMaterial;
        theBS.died = true;
        isDissolving = true;
        anim.enabled = false;
        theBS.antiThrustForce.SetValue(Vector2.zero);
    }

    protected virtual bool KnockBack(Vector2 _thrust)
    {
        float thurstX = Mathf.Abs(_thrust.x) - theBS.antiThrustForce.GetValue().x >= 0 ? _thrust.x - theBS.antiThrustForce.GetValue().x : 0f;
        float thurstY = _thrust.y - theBS.antiThrustForce.GetValue().y >= 0 ? _thrust.y - theBS.antiThrustForce.GetValue().y : 0f;
        Vector2 vectorThrust = new(thurstX, thurstY);

        if (vectorThrust.Equals(Vector2.zero)) return false;
        hurtingTime = theBS.timeHurt.GetValue();
        
        theRB.velocity = Vector2.zero;
        theRB.AddForce(vectorThrust);

        anim.SetTrigger("hurtStrigger");
        anim.SetBool("isAttack", false);
        anim.SetFloat("moveSpeed", 0f);
        return true;
    }

    public virtual void Damaged(int _damage, Vector2 _thrust, bool _isCritical)
    {
        if (isInvicible || _timeInvincible > 0)
        {
            _damage = 0;
        }
        TakeDamage(_damage, _isCritical);
        if (theBS.currentHP.GetValue() <= 0) Die();
        KnockBack(_thrust);
    }

    protected void SetInvincible(float _time)
    {
        _timeInvincible = _timeInvincible < _time ? _time : _timeInvincible;
    }
}
