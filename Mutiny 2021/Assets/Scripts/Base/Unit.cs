using UnityEngine;

public class Unit : MonoBehaviour
{
    [HideInInspector] public PlayerMovement thePM;
    private PlayerAttack thePA;
    private PlayerTouch thePT;
  
    private void Start()
    {
        thePM = GetComponent<PlayerMovement>();
        thePA = GetComponent<PlayerAttack>();
    }

    public void InputMove()
    {
        thePM.Move();
    }

    public void InputJump()
    {
        thePM.Jump();
    }

    public void InputAttack()
    {
        thePA.Attack();
    }

    public void InputDash()
    {
        thePM.InputDash();
    }
}

class InputDashCommmand : ICommand
{
    public void Execute(Unit unit)
    {
        unit.InputDash();
    }
}

class InputAttackCommand : ICommand
{
    public void Execute(Unit unit)
    {
        unit.InputAttack();
    }
}

class InputMoveCommand : ICommand
{
    public void Execute(Unit unit)
    {
        if (InputController.Instance.unitControl.thePM.isDashing) return;
        unit.InputMove();
    }
}

class InputJumpCommand : ICommand
{
    public void Execute(Unit unit)
    {
        unit.InputJump();
    }
}
