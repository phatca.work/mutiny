using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement: MonoBehaviour
{
    protected Animator anim;
    protected Rigidbody2D theRB;
    protected BaseStats theBS;

    [Header("Check grounded")]
    [HideInInspector] public Collider2D isGrounded;
    [SerializeField] private Transform groundCheckPoint;
    [SerializeField] private LayerMask whichIsGrounded;

    protected virtual void Start()
    {
        anim = GetComponent<Animator>();
        theRB = GetComponent<Rigidbody2D>();
        theBS = GetComponent<BaseStats>();
    }

    public bool CheckGrounded()
    {
        if (groundCheckPoint == null) return true;
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, .2f, whichIsGrounded);
        anim.SetBool("isGrounded", isGrounded);
        CheckTouchGround();
        return isGrounded == null;
    }

    private void CheckTouchGround()
    {
        if (isGrounded == null)
        {
            theBS.moveSpeed.RemoveValueFromList("Ground");
            return;
        }
        theBS.moveSpeed.AddValueByPercent("Ground", TouchGroundController.Instance.TouchGround(isGrounded.tag));
    }
}
