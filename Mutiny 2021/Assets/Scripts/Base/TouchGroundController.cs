using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchGroundController : Singleton<TouchGroundController>
{

    [Range(-100, 100)]
    [SerializeField]
    private int _water;


    [Range(-100, 100)]
    [SerializeField]
    private int _poision;

    public int TouchGround(string _nameGround)
    {
        return _nameGround switch
        {
            "Water" => _water,
            "Poision" => _poision,
            _ => 0,
        };
    }
}
