using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Skill : MonoBehaviour
{
    protected Rigidbody2D theRB;
    protected Collider2D theColl;

    [SerializeField] protected int damage;
    [SerializeField] protected float speed;
    [SerializeField] protected Attacker originOfDamage;
    [SerializeField] private string layerOfEnemy;

    protected virtual void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
        theColl = GetComponent<Collider2D>();
    }

    public virtual void Setup(int _damage, Attacker _originOfDamage)
    {
        damage = _damage;
        originOfDamage = _originOfDamage;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(LayerMask.NameToLayer(layerOfEnemy)))
        {
            TakeDamage(collision);
            Destroy();
        }
    }

   /* public virtual void TakeDamage(Collider2D collision)
    {
        originOfDamage.InflictDamage(collision, damage, Vector2.zero, false);
    }*/

    public virtual void TakeDamage(Collider2D collision, bool? isCrit = null)
    {
        bool _isCrit;
        if (isCrit.HasValue) {
            _isCrit = isCrit.Value;
        }
        else
        {
            _isCrit = false;
        }
        originOfDamage.InflictDamage(collision, damage, Vector2.zero, _isCrit);
    }

    public abstract void Destroy();

    protected void OnDestroy()
    {
        Destroy(gameObject);
    }
}
