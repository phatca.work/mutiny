using System;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

public enum EventID
{
    None = 0,
    EventPlayerInvincible,
    EventPlayerTakeHit,
    PickUpItem,
    Event2,
}

public class EventController : Singleton<EventController>
{
    public UnityEvent EventPlayerTakeHit;
    public UnityEvent<BaseStats> EventEnemyDie;
    public UnityEvent<int> EventChangeCoin;
}
