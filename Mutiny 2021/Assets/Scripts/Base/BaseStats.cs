using System;
using System.Collections.Generic;
using UnityEngine;

public class BaseStats : MonoBehaviour
{
    [SerializeField] private float _moveSpeed;
    public FloatStat moveSpeed = new();

    [SerializeField] private float _jumpForce;
    public FloatStat jumpForce = new();

    [SerializeField] private int _maxHP;
    public IntStat maxHP = new();

    [SerializeField] private int _currentHP;
    public IntStat currentHP = new();

    [SerializeField] private Vector2 _thrustForce;
    [HideInInspector] public Vector2Stat thrustForce;

    [SerializeField] private Vector2 _antiThrustForce;
    [HideInInspector] public Vector2Stat antiThrustForce;

    [SerializeField] private float _timeHurt;
    public FloatStat timeHurt = new();

    [SerializeField] private int _attackSpeed;
    public IntStat attackSpeed = new();

    [SerializeField] private int _attackDamage;
    public IntStat attackDamage = new();

    [SerializeField] private int _rateCritical;
    public IntStat rateCritical = new();

    [SerializeField] private int _coin;
    public IntStat coin = new();

    public bool died;

    private void Awake()
    {
        moveSpeed.SetBase(_moveSpeed);
        jumpForce.SetBase(_jumpForce);
        maxHP.SetBase(_maxHP);
        currentHP.SetBase(_currentHP);
        antiThrustForce = new();
        antiThrustForce.SetBase(_antiThrustForce);
        thrustForce = new();
        thrustForce.SetBase(_thrustForce);
        timeHurt.SetBase(_timeHurt);
        attackSpeed.SetBase(_attackSpeed);
        attackDamage.SetBase(_attackDamage);
        rateCritical.SetBase(_rateCritical);
        coin.SetBase(_coin);
    } 
}

public class Vector2Stat : MonoBehaviour
{
    private Vector2 _baseValue;
    private Vector2 _finalValue;
    private Dictionary<string, Vector2> _listValue;

    public void SetValue(Vector2 _value)
    {
        _listValue = new();
        _finalValue = _value;
    }

    public void AddValue(Vector2 _value)
    {
        _finalValue += _value;
    }

    public void SetBase(Vector2 _baseValue)
    {
        this._baseValue = new Vector2(_baseValue.x, _baseValue.y);
        this._finalValue = new Vector2(_baseValue.x, _baseValue.y);
    }

    public void AddValueToList(string _name, Vector2 _value)
    {
        if (_listValue.ContainsKey(_name))
        {
            if (_listValue[_name] != _value)
            {
                _finalValue += _value - _listValue[_name];
                _listValue[_name] = _value;
            }
            return;
        }
        _listValue.Add(_name, _value);
        _finalValue += _value;
    }

    public void AddValueToListByPercent(string _name, int _percent)
    {
        Vector2 _value = new Vector2(_baseValue.x * _percent / 100, _baseValue.y * _percent / 100);
        AddValueToList(_name, _value);
    }

    public Vector2 GetValue()
    {
        return _finalValue;
    }

    public void RemoveValueFromList(string _name)
    {
        if (!_listValue.ContainsKey(_name)) return;
        _finalValue -= _listValue[_name];
        _listValue.Remove(_name);
    }

    public void ResetValue()
    {
        _finalValue = _baseValue;
    }
}
public class IntStat
{
    private int _baseValue;
    private int _finalValue;
    private Dictionary<string, int> _listValue;

    public void SetValue(int _value)
    {
        _listValue = new();
        _finalValue = _value;
    }

    public void AddValue(int _value)
    {
        _finalValue += _value;
    }

    public void SetBase(int _baseValue)
    {
        this._baseValue = _baseValue;
        this._finalValue = _baseValue;
    }

    public void AddValueToList(string _name, int _value)
    {
        if (_listValue.ContainsKey(_name))
        {
            if (_listValue[_name] != _value)
            {
                _finalValue += _value - _listValue[_name];
                _listValue[_name] = _value;
            }
            return;
        }
        _listValue.Add(_name, _value);
        _finalValue += _value;
    }

    public void AddValueToListByPercent(string _name, int _percent)
    {
        int _value = Convert.ToInt32(Math.Round(((float)_baseValue * _percent) / 100));
        AddValueToList(_name, _value);
    }

    public int GetValue()
    {
        return _finalValue;
    }

    public void RemoveValueFromList(string _name)
    {
        if (!_listValue.ContainsKey(_name)) return;
        _finalValue -= _listValue[_name];
        _listValue.Remove(_name);
    }

    public void ResetValue()
    {
        _finalValue = _baseValue;
    }

    public float ScaleAdded()
    {
        return _finalValue / _baseValue;
    }
}
public class FloatStat
{
    private float _baseValue;
    private float _finalValue;
    private Dictionary<string, float> _listValue = new();

    public void SetValue(float _value)
    {
        _listValue = new();
        _finalValue = _value;
    }
    public void AddValue(float _value)
    {
        _finalValue += _value;
    }

    public void SetBase(float _value)
    {
        this._baseValue = _value;
        this._finalValue = _value;
    }

    public void AddValueToList(string _name, float _value)
    {
        if (_listValue.ContainsKey(_name))
        {
            if (_listValue[_name] != _value)
            {
                _finalValue += _value - _listValue[_name];
                _listValue[_name] = _value;
            }
            return;
        }
        _listValue.Add(_name, _value);
        _finalValue += _value;
    }

    public void AddValueByPercent(string _name, int _percent)
    {
        float _value = (float)Math.Round((_baseValue * _percent) / 100);
        AddValueToList(_name, _value);
    }

    public float GetValue()
    {
        return _finalValue;
    }

    public void RemoveValueFromList(string _name)
    {
        if (!_listValue.ContainsKey(_name)) return;
        _finalValue -= _listValue[_name];
        _listValue.Remove(_name);
    }

    public void ResetValue()
    {
        _finalValue = _baseValue;
    }

    public float ScaleAdded()
    {
        return _finalValue / _baseValue;
    }
}

