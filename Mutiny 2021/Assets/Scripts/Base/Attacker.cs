using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class Attacker : MonoBehaviour
{
    protected BaseStats theBS;
    protected Animator anim;

    [SerializeField] protected bool isAttacker = true;

    public Transform[] arrAttackPoint;
    public float[] arrAttackRange;
    public LayerMask enemyLayers;
    // Cached already struck objects of said attack to avoid overlapping attacks on same target
    [HideInInspector] public List<Collider2D> collidersDamaged;
    [HideInInspector] public int _attackIndex = 0;

    protected virtual void Start()
    {
        theBS = GetComponent<BaseStats>();
        anim = GetComponent<Animator>();
        collidersDamaged = new();
    }

    protected virtual void Update()
    {
        if (isAttacker)
        {
            if (anim.GetFloat("Weapon.Active") > 0f)
            {
                Attack(_attackIndex);
            }
        }
    }

    public virtual bool InflictDamage(Collider2D _enemy, int _damage, Vector2 _thrustForce, bool _critical)
    {
        if (_enemy.TryGetComponent(out Victim _victim))
        {
            _victim.Damaged(_damage, _thrustForce, _critical);
            return true;
        }
        return false;
    }

    public virtual bool InflictDamage(Collider2D _enemy)
    {
        if (_enemy.TryGetComponent(out Victim _victim))
        {
            _victim.Damaged
            (
            theBS.attackDamage.GetValue(),
            new Vector2(transform.rotation.y == 0f ? theBS.thrustForce.GetValue().x : -theBS.thrustForce.GetValue().x, theBS.thrustForce.GetValue().y),
            Random.Range(0, 100) < theBS.rateCritical.GetValue()
            );
            return true;
        }
        return false;
    }

    public virtual void Attack(int _attackIndex)
    {
        Collider2D[] colliderCount = Physics2D.OverlapCircleAll(arrAttackPoint[_attackIndex].position, arrAttackRange[_attackIndex], enemyLayers);
        for (int i = 0; i < colliderCount.Length; i++)
        {
            if (!collidersDamaged.Contains(colliderCount[i]) && colliderCount[i].TryGetComponent(out BaseStats _theBS) && !_theBS.died)
            {
                if (InflictDamage(colliderCount[i])) collidersDamaged.Add(colliderCount[i]);
            }
        }
    }

    protected virtual void OnDrawGizmosSelected()
    {
        if (arrAttackPoint.Length == 0 || arrAttackRange.Length == 0) return;
        for (int i = 0; i < arrAttackPoint.Length; i++)
        {
            Gizmos.DrawWireSphere(arrAttackPoint[i].position, arrAttackRange[i]);
        }
    }
}
