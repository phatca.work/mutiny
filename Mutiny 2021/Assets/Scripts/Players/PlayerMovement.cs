using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Movement
{
    [Header("Movement")]

    private bool canDoubleJump;

    public bool canDash = true;
    public bool canDoubleDash = false;
    public bool isDashing;
    [SerializeField] private float dashingForce;
    private float dashingTime = 0.2f;
    private float dashingCooldown = 1f;
    private float timeDashingCooldown;

    private float timeDashEffectSpawns;
    [SerializeField] private float startTimeDashEffectSpawns;
    [SerializeField] private GameObject GODashEffect;


    protected void FixedUpdate()
    {
        CheckGrounded();
        SetAnimFall();
        if (timeDashingCooldown > 0) timeDashingCooldown -= Time.deltaTime;
        if (isDashing) CreateDashEffect();
        else timeDashEffectSpawns = 0;
    }

    private void SetAnimFall()
    {
        anim.SetBool("fall", theRB.velocity.y < 0.1f);
        if (isGrounded) anim.SetBool("fall", false);
    }

    public virtual void Move()
    {
        theRB.velocity = new Vector2(Input.GetAxis("Horizontal") * theBS.moveSpeed.GetValue(), theRB.velocity.y);
        anim.SetFloat("moveSpeed", Mathf.Abs(theRB.velocity.x));

        if (theRB.velocity.x < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else if (theRB.velocity.x > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public virtual void InputDash()
    {
        if (!canDash && !canDoubleDash) return;
        if (timeDashingCooldown <= 0)
        {
            canDoubleDash = true;
            StartCoroutine(Dash());
        }
        else if (canDoubleDash)
        {
            canDoubleDash = false;
            StartCoroutine(Dash());
        }
    }

    public virtual IEnumerator Dash()
    {
        EventDispatcher.Instance.PostEvent(EventID.EventPlayerInvincible, dashingTime);
        timeDashingCooldown = dashingCooldown;
        isDashing = true;
        float originalGravity = theRB.gravityScale;
        theRB.gravityScale = 0f;
        theRB.velocity = new Vector2(transform.rotation.y == 0 ? dashingForce : -dashingForce, 0f);
        anim.SetBool("dash", true);
        yield return new WaitForSeconds(dashingTime);
        anim.SetBool("dash", false);
        theRB.gravityScale = originalGravity;
        isDashing = false;
    }



    private void CreateDashEffect()
    {
        if (timeDashEffectSpawns <= 0)
        {
            GODashEffect.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
            GameObject GO = Instantiate(GODashEffect, transform.position, transform.rotation);
            Destroy(GO, 0.5f);
            timeDashEffectSpawns = startTimeDashEffectSpawns;
        }
        else
        {
            timeDashEffectSpawns -= Time.deltaTime;
        }
    }

    public virtual void Jump()
    {
        if (isGrounded)
        {
            theRB.velocity = new Vector2(theRB.velocity.x, theBS.jumpForce.GetValue());
            canDoubleJump = true;
        }
        else if (canDoubleJump)
        {
            anim.SetTrigger("doubleJump");
            canDoubleJump = false;
            theRB.velocity = new Vector2(theRB.velocity.x, theBS.jumpForce.GetValue());
        }
    }
}
