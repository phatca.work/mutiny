using UnityEngine;

public abstract class PlayerAttack : Attacker
{
    protected StateMachine theSM;

    protected override void Start()
    {
        base.Start();
        theSM = GetComponent<StateMachine>();
    }

    public abstract void Attack();

    private void PostEventTakeHit()
    {
        EventController.Instance.EventPlayerTakeHit.Invoke();
    }
}
