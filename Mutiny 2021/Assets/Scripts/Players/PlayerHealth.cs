using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerHealth : Victim
{
    protected override void Start()
    {
        base.Start();
        EventDispatcher.Instance.RegisterListener(EventID.EventPlayerInvincible, (param) => SetInvincible((float)param));
    }

    protected override void Update()
    {
        base.Update();
        
        if (Input.GetKeyDown(KeyCode.RightControl))
        {
            Damaged(5, new Vector2(0, 0), false);
        }
    }

    public override void Damaged(int _damage, Vector2 _thrust, bool _isCritical)
    {
        base.Damaged(_damage, _thrust, _isCritical);
    }

    protected override bool KnockBack(Vector2 _thrust)
    {
        if (base.KnockBack(_thrust))
        {
            InputController.Instance.stopMove = theBS.timeHurt.GetValue();
        }
        return true;
    }

    
}
