using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Progress;

public class PlayerTouch : MonoBehaviour
{
    [SerializeField] private GameObject GOListItem;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out ItemObject itemTouched))
        {
            UIInforItem.Instance.TouchItem(itemTouched);
            
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out ItemObject itemTouched))
        {
            UIInforItem.Instance.UnTouchItem();
        }
    }
}
