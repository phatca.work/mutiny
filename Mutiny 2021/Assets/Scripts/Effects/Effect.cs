using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    private void OnDisable()
    {
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Destroy(gameObject);
    }
}
