using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider slider;
    //[SerializeField] private Gradient gradient;
    [SerializeField] private Image frontFill, backFill, border, Iout;
    [SerializeField] private Material material;

    [SerializeField] private int HPByGrid;
    [SerializeField] private GameObject gridPrarent;
    [SerializeField] private GameObject grid;

    private float lerpTimer;

    private void Start()
    {
        Material _newMater = new(material);
        frontFill.material = _newMater;
        backFill.material = _newMater;
        border.material = _newMater;
        Iout.material = _newMater;
    }

    private void FixedUpdate()
    {
        UpdateBackFillHealth();
    }

    public void DissolvingHealthBar(float fade)
    {
        frontFill.material.SetFloat("_Fade", fade);
        backFill.material.SetFloat("_Fade", fade);
        border.material.SetFloat("_Fade", fade);
        Iout.material.SetFloat("_Fade", fade);
        for (int i = 0; i < gridPrarent.transform.childCount; i++)
        {
            gridPrarent.transform.
                GetChild(gridPrarent.transform.childCount - i - 1)
                .GetComponent<Image>()
                .material.SetFloat("_Fade", fade);
        }
    }

    private Transform GetLastChildGridParent()
    {
        int numberChild = gridPrarent.transform.childCount;
        if (numberChild == 0) return null;
        return gridPrarent.transform.GetChild(numberChild - 1);
    }

    private int SetHPByGrid(int _hp)
    {
        while (_hp > HPByGrid * 10)
        {
            if (_hp > HPByGrid * 10)
            {
                HPByGrid *= 2;
            }
        }
        return HPByGrid;
    }

    private void SetGrid(int _hp)
    {
        float _number = (_hp / SetHPByGrid(_hp));
        int _countGrid = _number % 1 > 0 ? (int)_number + 1 : (int)_number;
        if (GetLastChildGridParent() != null)
        {
            GetLastChildGridParent().GetComponent<Image>().enabled = true;
        }
        int childCount = gridPrarent.transform.childCount;
        if (_countGrid > childCount)
        {
            for (int i = 0; i < _countGrid - childCount; i++)
            {
                var itemGrid = Instantiate(grid, gridPrarent.transform);
                itemGrid.GetComponent<Image>().material = new Material(material);
            }
            GetLastChildGridParent().GetComponent<Image>().enabled = false;
        }
        else
        {
            for (int i = 0; i < childCount - _countGrid; i++)
            {
                GetLastChildGridParent().gameObject.SetActive(false);
            }
        }
    }

    public void SetMaxHealth(int _health)
    {
        slider.maxValue = _health;
        slider.value = _health;
        SetGrid(_health);
        //fill.color = gradient.Evaluate(1f);
    }

    public void SetHealth(int _health)
    {
        slider.value = _health;
        lerpTimer = 0f;
        //fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    private void UpdateBackFillHealth()
    {
        float fill = backFill.fillAmount;
        float fraction = slider.value / slider.maxValue;
        if(fill > fraction)
        {
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / 2f;
            backFill.fillAmount = Mathf.Lerp(fill, fraction, percentComplete);
        }
    }
}
