using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : Singleton<InputController>
{
    public Unit unitControl;
    public KeyCode keyTab;
    public KeyCode keyJump;
    public KeyCode keyAttack;
    public KeyCode keyDash;
    public KeyCode keyPickupItem;

    public bool stopWhenAttack;
    public float stopMove;

    private readonly InputAttackCommand inputAttack = new();
    private readonly InputMoveCommand inputMove = new();
    private readonly InputJumpCommand inputJump = new();
    private readonly InputDashCommmand inputDash = new();

    private void Update()
    {
        ProcessInput();
    }
    private void ProcessInput()
    {
        if (stopMove > 0)
        {
            stopMove -= Time.deltaTime;
            return;
        }

        if (Input.GetKeyDown(keyTab))
        {
            UIListItemController.Instance.InteractTableListItemUI();
        }

        if (unitControl.thePM.isDashing) return;

        if (Input.GetKeyDown(keyJump))
        {
            inputJump.Execute(unitControl);
        }

        if (Input.GetKeyDown(keyDash))
        {
            inputDash.Execute(unitControl);
        }

        if (stopWhenAttack) return;

        if (Input.GetKeyDown(keyAttack))
        {
            inputAttack.Execute(unitControl);
        }

        inputMove.Execute(unitControl);

        
    }
}



