public interface ICommand
{
    public void Execute(Unit unit);
}
