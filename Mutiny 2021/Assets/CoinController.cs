using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    void Start()
    {
        EventController.Instance.EventEnemyDie.AddListener(param => CreateCoint(param));
    }

    private void CreateCoint(BaseStats param)
    {
        //10 50 100 200
        int totalCoin = param.coin.GetValue();
        int bag = totalCoin / 200;
        int pile = (totalCoin % 200) / 100;
        int gold = (totalCoin % 100) / 50;
        int cent = (totalCoin % 50) / 10 > 0 ? ((totalCoin % 50) / 10) + 1 : (totalCoin % 50) / 10;
        

        for (int i = 0; i < bag; i++)
        {
            PoolABagOfMoney.instance.GetObjectFromPool(param.transform.position, 200);
            totalCoin -= 200;
        }
        for (int i = 0; i < pile; i++)
        {
            PoolAPileOfMoney.instance.GetObjectFromPool(param.transform.position, 100);
            totalCoin -= 100;
        }
        for (int i = 0; i < gold; i++)
        {
            PoolGoldCoin.instance.GetObjectFromPool(param.transform.position, 50);
            totalCoin -= 50;
        }
        for (int i = 0; i <= cent; i++)
        {
            if(i != cent)
            {
                PoolCentCoin.instance.GetObjectFromPool(param.transform.position, 10);
            }
            else
            {
                PoolCentCoin.instance.GetObjectFromPool(param.transform.position, totalCoin);
            }
            
        }
    }
}
